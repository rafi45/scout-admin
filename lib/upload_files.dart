import 'package:admin/resources/color_manager.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


class UploadFiles extends StatelessWidget {
  const UploadFiles({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorManager.scaffoldBlack,
      body: Column(
        children: [
          const SizedBox(height: 20.0),
          Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                      height: 40.0,
                      width: 40.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7.0),
                          color: ColorManager.darkred
                      ),
                      child: Center(
                          child: IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: const Icon(Icons.arrow_back_ios), color: Colors.white,
                          ),
                      ),
                  ),
                  Text('Upload Files',
                      style: GoogleFonts.montserrat(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400,
                          textStyle: const TextStyle(color: Colors.white),
                      ),
                  ),
                  Container(
                    height: 40.0,
                    width: 40.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7.0),
                        color: const Color(0xFF353535)),
                    child:  Center(
                      child:IconButton(
                        onPressed: (){

                          showDialog(
                            context: context,
                            builder: (context) {
                              return uploadFileDialog();
                            },
                          );
                      }, icon: const Icon(Icons.add, color: Colors.white),)
                    ),
                  ),
                ],
              ),
           ),

          Expanded(
            child: ListView.separated(
                itemBuilder: (context, index) => myUploadedItem(0.3),
                separatorBuilder: (context, index) => const SizedBox(height: 2),
                itemCount: 10
            ),
          )
        ],
      ),
    );
  }

  myUploadedItem( double downloadPercentage) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text('Qays alsarry',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15,color: Colors.white),),
          const SizedBox(height: 10),
          Container(
            height: 80,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
             // border: Border.all(color: Colors.grey),
              color: Colors.grey[800],
              borderRadius: BorderRadius.circular(20)
            ),
            child: Row(
              children: [
              Icon(Icons.file_copy,
              //  color: Colors.primaries[Random().nextInt(Colors.primaries.length)],
                color: ColorManager.darkred,
                size: 40,
              ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text('journey.pdf ',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey),),
                      const SizedBox(height: 10,),
                      LinearProgressIndicator(
                        value: downloadPercentage,
                        //color: Colors.primaries[Random().nextInt(Colors.primaries.length)],
                        color: ColorManager.primary,
                        backgroundColor: ColorManager.lightprimary,
                      ),
                    ],
                  ),
                ),
                const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('350KB',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey),),
                    Text(' 30% ',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey),),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget uploadFileDialog() {
    return AlertDialog(
      backgroundColor: ColorManager.lightGrey,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
        // side: BorderSide(
        //   color: Colors.yellow
        // )
      ),
      content: const Column(
        children: [
          Text('File upload'),

        ],
      ),
    );
  }
}
