import 'package:admin/preferences_manager/preferences_manager.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'admin_constants/admin_constants.dart';
import 'app/app.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp( MyApp());
  DioHelper.init();
  await PreferencesManager.init();
  token = PreferencesManager.getData('api_token')!=null?PreferencesManager.getData('api_token'):'';
  print('token is : $token' );
}

