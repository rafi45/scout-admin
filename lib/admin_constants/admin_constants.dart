import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:dio/dio.dart';

import '../http_constants.dart';

void showToast(String message , ToastStates state){
  print('show toast');
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 3,
      webBgColor: chooseWebToastColor(state),
      webPosition: 'center',
      backgroundColor: chooseToastColor(state),
      textColor: Colors.white,
      fontSize: 16.0
  );
}
enum ToastStates {
  Success,
  Error,
  Warning
}

String chooseWebToastColor(ToastStates state){
  switch(state){
    case ToastStates.Success :
      print('Colors.green');
      return 'green';
      break;
    case ToastStates.Warning :
      print('Colors.yellow');

      return 'yellow';

      break;
    case ToastStates.Error :
      print('Colors.red');

      return 'red';
      break;
  }
}
Color chooseToastColor(ToastStates state){
  switch(state){
    case ToastStates.Success :
      print('Colors.green');
      return Colors.green;
      break;
    case ToastStates.Warning :
      print('Colors.yellow');

      return Colors.yellow;

      break;
    case ToastStates.Error :
      print('Colors.red');

      return Colors.red;
      break;
  }
}

String token = '';



class DioHelper {
  static late Dio dio;
  static init() {
    dio = Dio(
        BaseOptions(
            baseUrl: baseUrl,
            receiveDataWhenStatusError: true,
            headers: {
              'Accept': 'application/json',
              'Content-Type':'application/json'
            }
        )
    );
  }


  static Future<Response> postData({
    required String url,
    Map<String,dynamic> ? query,
    required Map<String,dynamic> data,
    String ? token,
  }) async{
    dio.options.headers = {
      'Accept': 'application/json',
      'Content-Type':'application/json',
      'Authorization' : token
    };
    return await dio.post(
      url,
      options:  Options(
        followRedirects: false,
        // will not throw errors
        validateStatus: (status) => true,
      ),
      queryParameters: query,
      data: data,
    );
  }



  static Future<Response> postDataImage({
    required String url,
    Map<String,dynamic> ? query,
    required FormData data,
    String ? token,
  }) async{
    dio.options.headers = {
      'Accept': 'application/json',
      'Content-Type':'application/json',
      'Authorization' : token
    };
    return await dio.post(
      url,
      options:  Options(
        followRedirects: false,
        // will not throw errors
        validateStatus: (status) => true,
      ),
      queryParameters: query,
      data: data,
    );
  }



  static Future<Response> getData({
    required String url,
    Map<String, dynamic> ?  query,
    String ? token,
  }) async{
    dio.options.headers = {
      'Accept': 'application/json',
      'Content-Type':'application/json',
      'Authorization' : token
    };
    return await dio.get(
        url,
        options:  Options(
          followRedirects: false,
          // will not throw errors
          validateStatus: (status) => true,
        ),
        queryParameters: query
    );
  }
}
