import 'dart:convert';
import 'dart:io';

import 'package:admin/http_constants.dart';
import 'package:http/http.dart' as http;

import 'package:http_parser/http_parser.dart';

class Http {


  static Future<http.Response> postData({
    required String path,
    required Map<String, dynamic> map,
    String ? token
  }) async {
    return await http.post(
      Uri.parse(baseUrl + path),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        "Authorization": 'Bearer $token' ?? "",
      },
      body: jsonEncode(map),
    );
  }


  static Future<http.Response> getData({
    required String path,
    Map<String, dynamic> ? map,
    String ? token,
  }) async {
    return await http.get(
      Uri.parse(baseUrl + path),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        "Authorization": 'Bearer $token' ?? ""
      },
    );
  }

  static Future<http.Response> deleteData({
    required String path,
    Map<String, dynamic> ? map,
    Map<String, dynamic> ? query,
    String ? token,
  }) async {
    return await http.delete(
      Uri.parse(baseUrl + path),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        "Authorization": 'Bearer $token' ?? ""
      },
    );
  }

  static Future postDataImage({
    required String path,
    required String token,
    required File file,
    required String fileName,
    required Map<String, String> map,
  }) async{
    print('1');
    var request = http.MultipartRequest(
      'POST', Uri.parse(baseUrl + path),
    );
    print('2');
    Map<String,String> headers={
      'Content-Type': 'application/json; charset=UTF-8',
      "Authorization": token ?? ""
    };
    print(3);
    request.files.add(
      http.MultipartFile(
        'file',
        file.readAsBytes().asStream(),
        file.lengthSync(),
        filename: fileName,
        contentType: MediaType('image','jpeg'),
      ),
    );
    print(4);
    request.headers.addAll(headers);
    print(5);
    request.fields.addAll(map);
    print('6 the end');
  }

  static Future postDataImage1({
    required String path,
    required String token,
   required File image,
   required String fileName,
    required Map<String, String> map,
  }) async{
    print(1);
    var request = http.MultipartRequest(
      'POST', Uri.parse(baseUrl + path),
    );
    print(2);
    Map<String,String> headers={
      'Content-Type': 'application/json; charset=UTF-8',
      "Authorization": token ?? ""
    };
    print(3);
    var file = await http.MultipartFile.fromPath('img_url', 'image.png');
    print(4);
    request.files.add(file);
    print(5);
    request.headers.addAll(headers);
    print(6);
    request.fields.addAll(map);
    print(7);
  }
}

