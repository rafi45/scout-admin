import '../model/delete_leader_model.dart';

abstract class AdminStates {}

class InitialAdminState extends AdminStates{}

class ResetSearchBarState extends AdminStates{}
class ExpandSearchBarState extends AdminStates{}



class SelectNewBranchState extends AdminStates{}
class GiveLibrarianPropertyState extends AdminStates{}




class SuccessPickedDateState extends AdminStates{}
class ErrorPickedDateState extends AdminStates{}


class SuccessPickedProfileImage extends AdminStates{}
class ErrorPickedProfileImage extends AdminStates{}


class LoadingCreateLeader extends AdminStates{}
class SuccessCreateLeader extends AdminStates{}
class ErrorCreateLeader extends AdminStates{}

class LoadingGetAllLeaders extends AdminStates{}
class SuccessGetAllLeaders extends AdminStates{}
class ErrorGetAllLeaders extends AdminStates{}


class LoadingDeleteLeader extends AdminStates{}
class SuccessDeleteLeader extends AdminStates{
  DeleteLeaderModel  deleteLeaderModel;
  SuccessDeleteLeader(this.deleteLeaderModel);
}
class ErrorDeleteLeader extends AdminStates{}