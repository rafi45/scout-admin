import 'dart:html';
import 'dart:typed_data';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'dart:convert';
import 'package:http_parser/http_parser.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';


import '../../../http_manager.dart';
import '../../admin_constants/admin_constants.dart';
import '../../http_constants.dart';
import '../../resources/color_manager.dart';
import '../model/create_leader_model.dart';
import '../model/delete_leader_model.dart';
import '../model/get_all_leaders_model.dart';
import 'admin_home_statese.dart';

class AdminCubit extends Cubit<AdminStates> {
  AdminCubit() : super(InitialAdminState());

  static AdminCubit get(context) {
    return BlocProvider.of(context);
  }

  TextEditingController searchController = TextEditingController();
  bool folded = true;


  void resetSearchBar() {
    if (searchController.text.isEmpty) {
      folded = !folded;
      print(
          'fooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooolded is : $folded');
      emit(ResetSearchBarState());
    }
  }

  void expandSearchBar() {
    folded = !folded;
    print(
        'fooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooolded is : $folded');
    emit(ExpandSearchBarState());
  }


  // String dropdownValue = 'Flowers Branch';
  // List<String> items = ['Flowers Branch', 'Cubs Branch', 'Rangers Branch', 'Advanced Branch'];
  //
  // void selectBranch(String newBranch){
  //   dropdownValue = newBranch;
  //   print(dropdownValue);
  //   emit(SelectNewBranchState());
  // }


  bool isLibrarian = false;
  List<bool> librarians = [false, true];

  giveLibrarianProperty(bool newValue) {
    isLibrarian = newValue;
    emit(GiveLibrarianPropertyState());
  }


  // Future<void> createLeader(){
  //   emit(LoadingCreateLeaderState());
  //   Http.postData(
  //       path: createLeaderPath,
  //       map: {
  //
  //       }
  //   );
  //
  //   return ;
  // }


  var dateController = TextEditingController();

  Future pickDate(context) async {
    DateTime? pickedDate = await showDatePicker(
      context: context,

      initialDate: DateTime.now(),
      //get today's date
      firstDate: DateTime(1900),
      //DateTime.now() - not to allow to choose before today.
      lastDate: DateTime(2024),
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            colorScheme: ColorScheme.light(
              primary: ColorManager.darkred, // <-- SEE HERE
              onPrimary: ColorManager.primary, // <-- SEE HERE
              onSurface: ColorManager.primary, // <-- SEE HERE
            ),
            textButtonTheme: TextButtonThemeData(
              style: TextButton.styleFrom(
                primary: ColorManager.darkred, // button text color
                textStyle: const TextStyle(
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          child: child!,
        );
      },
    );
    if (pickedDate != null) {
      print(
          pickedDate); //get the picked date in the format => 2022-07-04 00:00:00.000
      String formattedDate = DateFormat('yyyy/MM/dd').format(
          pickedDate); // format date in required form here we use yyyy-MM-dd that means time is removed
      print(
          formattedDate); //formatted date output using intl package =>  2022-07-04
      //You can format date as per your need

      dateController.text =
          formattedDate; //set formatted date to TextField value.
      emit(SuccessPickedDateState());
    } else {
      emit(ErrorPickedDateState());
      print("Date is not selected");
    }
  }


  List<int> branches = [];
  bool selectFlowers = false;
  bool selectCubs = false;
  bool selectRangers = false;
  bool selectAdvanced = false;

  selectBranch1(String branch) {
    if (branch == 'Flowers') {
      if (!branches.contains(1)) {
        branches.add(1);
        selectFlowers = !selectFlowers;
      } else {
        selectFlowers = !selectFlowers;
        branches.remove(1);
      }
      emit(SelectNewBranchState());
    }
    if (branch == 'Cubs') {
      if (!branches.contains(2)) {
        branches.add(2);
        selectCubs = !selectCubs;
      } else {
        selectCubs = !selectCubs;
        branches.remove(2);
      }
      emit(SelectNewBranchState());
    }
    if (branch == 'Rangers') {
      if (!branches.contains(3)) {
        branches.add(3);
        selectRangers = !selectRangers;
      } else {
        selectRangers = !selectRangers;
        branches.remove(3);
      }
      emit(SelectNewBranchState());
    }
    if (branch == 'Advanced') {
      if (!branches.contains(4)) {
        branches.add(4);
        selectAdvanced = !selectAdvanced;
      } else {
        selectAdvanced = !selectAdvanced;
        branches.remove(4);
      }
      emit(SelectNewBranchState());
    }
    print(branches.map((e) => e));
  }

  // var picker = ImagePicker();
  //  File ? profileImage;
  //  File emptyFile = File('');
  //
  // Future<void> getProfileImage() async{
  //   final pickedFile = await picker.pickImage(
  //     source: ImageSource.gallery,
  //   );
  //   if(pickedFile != null){
  //     profileImage = File(pickedFile.path);
  //     emit(SuccessPickedProfileImage());
  //   }else{
  //     emit(ErrorPickedProfileImage());
  //   }
  // }


  var firstNameController = TextEditingController();
  var lastNameController = TextEditingController();
  var userNameController = TextEditingController();
  var passwordController = TextEditingController();
  var passwordConfirmationController = TextEditingController();
  var guardianController = TextEditingController();
  var phoneController = TextEditingController();
  var addressController = TextEditingController();
  var landLineController = TextEditingController();
  var hobbiesController = TextEditingController();


//
//   CreateLeaderModel ?createLeaderModel;
//   void uploadDataImage({
//     required String firstName,
//     required String lastName,
//     required String userName,
//     required String password,
//     required String passwordConfirmation,
//     required String phone,
//     required String address,
//     required String guardian,
//     required String landLineTelephone,
//     required File file,
//     required String hobbies,
//     required bool librarian,
//     required String birthdate,
//     required List<int> branch,
// }) async {
//
//     emit(LoadingCreateLeader());
//     String fileName = file.path.split('/').last;
//
//     FormData formData = FormData.fromMap(
//         {
//         // var multipartFile = http.MultipartFile.fromBytes(
//         // 'img_url',
//         // data,
//         // filename: file.name,
//         // contentType: MediaType('image', 'jpg'),
//         // );
//         //   "image_url": await MultipartFile.fromFile(
//         //       file.path, filename: fileName
//         // ),
//           "image_url": await MultipartFile.fromBytes(value),
//           "first_name":firstName,
//           "last_name":lastName,
//           "username":userName,
//           "password":password,
//           "password_confirmation":passwordConfirmation,
//           "phone":phone,
//           "address":address,
//           "guardian":guardian,
//           "land_line_telephone":landLineTelephone,
//           "hobbies":hobbies,
//           "librarian":isLibrarian,
//           "birthdate":birthdate,
//           "branch[0]":branch[0],
//           "branch[1]":branch[1],
//           "branch[2]":branch[2],
//           "branch[3]":branch[3]
//         }
//     );
//     DioHelper.postDataImage(
//         url: createLeaderPath,
//         data: formData
//     ).then((value) {
//       print(value.data);
//       print(value.data.toString());
//       createLeaderModel = CreateLeaderModel.fromJson(value.data);
//       print('qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq');
//       emit(SuccessCreateLeader());
//
//     }).catchError((error){
//
//       print('errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrorrrrrrrrrrrr');
//       emit(ErrorCreateLeader());
//       print(error.toString());
//
//     });
//
//   }

//   late CreateLeaderModel createLeaderModel;
//    Future createLeader ({
//     required String firstName,
//     required String lastName,
//     required String userName,
//     required String password,
//     required String passwordConfirmation,
//     required String phone,
//     required String address,
//     required String guardian,
//     required String landLineTelephone,
//      required File file,
//     required String hobbies,
//     required bool librarian,
//     required String birthdate,
//     required List<int> branch,
//    }) async{
//     emit(LoadingCreateLeader());
//     print('LoadingCreateLeader');
//     return await Http.postDataImage1(
//         path: createLeaderPath,
//         token: token,
//        image: file,
//        fileName: file.path.split('/').last,
//         map: {
//           "first_name":firstName,
//           "last_name":lastName,
//           "username":userName,
//           "password":password,
//           "password_confirmation":passwordConfirmation,
//           "phone":phone,
//           "address":address,
//           "guardian":guardian,
//           "land_line_telephone":landLineTelephone,
//           "image_url":"njnjnjj",
//           "hobbies":hobbies,
//           "librarian":isLibrarian?'1':'0',
//           "birthdate":birthdate,
//           "branch[0]":"${branch[0]}",
//           "branch[1]":"${branch[1]}",
//           "branch[2]":"${branch[2]}",
//           "branch[3]":"${branch[3]}",
//         }
//     ).then((value) {
//       print('SuccessCreateLeader');
//       createLeaderModel = CreateLeaderModel.fromJson(jsonDecode(value.body));
//       print(value);
//       emit(SuccessCreateLeader());
//    }).catchError((error){
//      print('ErrorCreateLeader');
//      emit(ErrorCreateLeader());
//   print(error.toString());
//  });
// }


  pickFile() async {
    FileUploadInputElement uploadInput = FileUploadInputElement();
    uploadInput.click();

    // Listen for changes on the file input element
    uploadInput.onChange.listen((e) {
      // read file content as dataURL
      final files = uploadInput.files;
      if (files!.length == 1) {
        final file = files[0];
        FileReader reader = FileReader();
        reader.onLoadEnd.listen((e) {
          //  uploadedImage = reader.result as Uint8List ;
          // _uploadData(reader.result, file);
          createLeaderApi(reader.result, file);
          print(
              'jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj');
          emit(SuccessPickedProfileImage());
        });
        reader.onError.listen((fileEvent) {
          var option1Text = "Some Error occured while reading the file";
          print(
              'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttjjjjjjj');

          emit(ErrorPickedProfileImage());
        });
        reader.readAsArrayBuffer(file);
      }
    });
  }

  //
  // void _uploadData(dynamic data, File file ) async {
  //   // Create a multipart request
  //
  //   print(1);
  //   var request = http.MultipartRequest(
  //       "POST", Uri.parse(baseUrl + createLeaderPath),
  //   );
  //   print(2);
  //   // Add text fields to the request
  //   request.fields["first_name"] = "rafishsh";
  //   request.fields["last_name"] = "shoufan";
  //   request.fields["username"] = "rafi-shoufan";
  //   request.fields["password"] = "12345678";
  //   request.fields["password_confirmation"] = "12345678";
  //   request.fields["phone"] = "123456789";
  //   request.fields["address"] = "damascus";
  //   request.fields["guardian"] = "souhail111";
  //   request.fields["land_line_telephone"] = "shoufan111";
  //   //   request.fields["image_url"] = "shoufan";
  //   request.fields["hobbies"] = "swimming";
  //   request.fields["librarian"] = '$isLibrarian';
  //   request.fields["birthdate"] = "shoufan111";
  //   request.fields["branches[0]"] = "1";
  //   request.fields["branches[1]"] = "2";
  //   request.fields["branches[2]"] = "3";
  //   request.fields["branches[3]"] = "4";
  //   // Add an authorization header to the request
  //   request.headers.addAll({
  //     "Authorization" : 'Bearer $token' ,
  //   //  "Content-Type" : 'multipart/form-data; boundary=boundary',
  //     'Content-Type': 'application/json; charset=UTF-8',
  //   });
  //  //  request.headers["Authorization"] = token;
  //  // // request. headers['Content-Type'] = 'multipart/form-data; boundary=boundary';
  //  //  request. headers['Content-Type'] = 'application/json; charset=UTF-8';
  //   print(3);
  //   // Create a multipart file using the data from the selected file
  //   var multipartFile = http.MultipartFile.fromBytes(
  //     'img_url',
  //     data,
  //     filename: file.name,
  //     contentType: MediaType('image', 'jpg'),
  //   );
  //   print(4);
  //   // Add the multipart file to the request
  //   request.files.add(multipartFile);
  //   print(5);
  //   // Send the request
  //   var response = await request.send().then((value) {
  //     print(value.stream.toString());
  //     print(value.stream);
  //   }).catchError((error){
  //     print('error');
  //     print(56);
  //     print(error.toString());
  //   });
  //   print(6);
  //   print(response.toString());
  //   print(response);
  //   print(response);
  //   // Handle the response
  //   if (response.statusCode == 200) {
  //     print("Upload successful");
  //     print(7);
  //     response.stream.transform(utf8.decoder).listen((value) {
  //       print(value);
  //       print(8);
  //     });
  //   } else {
  //     print(9);
  //     print("Upload failed");
  //   }
  // }

  createLeaderApi(dynamic data, File file) async {
    var headers = {
      'Authorization': 'Bearer $token'
    };
    var request = http.MultipartRequest(
        'POST', Uri.parse('http://127.0.0.1:8000/api/admin/leader/register'));
    request.fields.addAll({
      'first_name': 'qays7878',
      'last_name': 'alsarydrdr',
      'username': 'qays-alsary1111k1115m65k',
      'password': '12345678h',
      'password_confirmation': '12345678h',
      'phone': '123456789',
      'address': 'suedjkja',
      'guardian': 'murselkjkj',
      'land_line_telephone': '4418999m',
      'hobbies': 'readingmmm',
      'librarian': '0',
      'birthdate': '1999/9/10',
      'branch[0]': '1',
      'branch[1]': '4',
      'branch[2]': '2',
      'branch[3]': '3'
    });
    request.files.add(await http.MultipartFile.fromBytes(
      'img_url',
      data,
      filename: file.name,
      contentType: MediaType('image', 'jpg'),
    ));
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    print(response.statusCode);
    print(response.toString());
    print(response);
    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      print('success');
    }
    else {
      print('error');
      print(response.reasonPhrase);
    }
  }



  GetAllLeadersModel ? getAllLeadersModel;

  Future getAllLeaders({String? text}) async {
    emit(LoadingGetAllLeaders());
    Http.getData(
        path: text == null
            ? getAllLeadersPath
            : '${getAllLeadersPath}?first_name=$text',
        token: token
    ).then((value) {
      print('success');
      print('token is : $token');
      print(value.body);
      getAllLeadersModel = GetAllLeadersModel.fromJson(jsonDecode(value.body));
      emit(SuccessGetAllLeaders());
    }).catchError((error) {
      print('error get all  leaders');
      print(error.toString());
      emit(ErrorGetAllLeaders());
    });
  }


  DeleteLeaderModel ? deleteLeaderModel;

  Future deleteLeader(int id) async {
    emit(LoadingDeleteLeader());
    Http.deleteData(
        path: '$deleteLeaderPath$id',
        token: token
    ).then((value) {
      print(value.body);
      deleteLeaderModel = DeleteLeaderModel.fromJson(jsonDecode(value.body));
      emit(SuccessDeleteLeader(deleteLeaderModel!));
      getAllLeaders();
    }).catchError((error) {
      print(error.toString());
      emit(ErrorDeleteLeader());
    });
  }

}