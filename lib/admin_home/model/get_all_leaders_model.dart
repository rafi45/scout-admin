class GetAllLeadersModel {
  List<Data>? data;
  String? message;
  int? statusCode;

  GetAllLeadersModel({this.data, this.message, this.statusCode});

  GetAllLeadersModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
    message = json['message'];
    statusCode = json['status code'];
  }


}

class Data {
  int? id;
  String? firstName;
  String? lastName;
  String? address;
  String? imageUrl;
  String? guardian;
  String? phone;
  String? birthdate;
  String? landLineTelephone;
  String? hobbies;
  String? username;
  int? librarian;
  int? roleId;
  List<Branch>? branch;

  Data(
      {this.id,
        this.firstName,
        this.lastName,
        this.address,
        this.imageUrl,
        this.guardian,
        this.phone,
        this.birthdate,
        this.landLineTelephone,
        this.hobbies,
        this.username,
        this.librarian,
        this.roleId,
        this.branch});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    address = json['address'];
    imageUrl = json['image_url'];
    guardian = json['guardian'];
    phone = json['phone'];
    birthdate = json['birthdate'];
    landLineTelephone = json['land_line_telephone'];
    hobbies = json['hobbies'];
    username = json['username'];
    librarian = json['librarian'];
    roleId = json['role_id'];
    if (json['branch'] != null) {
      branch = <Branch>[];
      json['branch'].forEach((v) {
        branch!.add(new Branch.fromJson(v));
      });
    }
  }


}

class Branch {
  int? id;
  String? name;

  Branch({this.id, this.name});

  Branch.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}