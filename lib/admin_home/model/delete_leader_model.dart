class DeleteLeaderModel {
  bool? status;
  String? message;
  int? statusCode;

  DeleteLeaderModel({this.status, this.message, this.statusCode});

  DeleteLeaderModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    statusCode = json['status code'];
  }

}
