class CreateLeaderModel {
  Data? data;
  String? message;
  int? statusCode;

  CreateLeaderModel({this.data, this.message, this.statusCode});

  CreateLeaderModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ?  Data.fromJson(json['data']) : null;
    message = json['message'];
    statusCode = json['status code'];
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   if (this.data != null) {
  //     data['data'] = this.data!.toJson();
  //   }
  //   data['message'] = this.message;
  //   data['status code'] = this.statusCode;
  //   return data;
  // }
}

class Data {
  String? firstName;
  String? lastName;
  String? username;
  String? phone;
  String? address;
  String? guardian;
  String? landLineTelephone;
  String? hobbies;
  String? librarian;
  String? birthdate;
  String? imageUrl;
  int? roleId;
  int? id;

  Data(
      {this.firstName,
        this.lastName,
        this.username,
        this.phone,
        this.address,
        this.guardian,
        this.landLineTelephone,
        this.hobbies,
        this.librarian,
        this.birthdate,
        this.imageUrl,
        this.roleId,
        this.id});

  Data.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    lastName = json['last_name'];
    username = json['username'];
    phone = json['phone'];
    address = json['address'];
    guardian = json['guardian'];
    landLineTelephone = json['land_line_telephone'];
    hobbies = json['hobbies'];
    librarian = json['librarian'];
    birthdate = json['birthdate'];
    imageUrl = json['image_url'];
    roleId = json['role_id'];
    id = json['id'];
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['first_name'] = this.firstName;
  //   data['last_name'] = this.lastName;
  //   data['username'] = this.username;
  //   data['phone'] = this.phone;
  //   data['address'] = this.address;
  //   data['guardian'] = this.guardian;
  //   data['land_line_telephone'] = this.landLineTelephone;
  //   data['hobbies'] = this.hobbies;
  //   data['librarian'] = this.librarian;
  //   data['birthdate'] = this.birthdate;
  //   data['image_url'] = this.imageUrl;
  //   data['role_id'] = this.roleId;
  //   data['id'] = this.id;
  //   return data;
  // }
}


