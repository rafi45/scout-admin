import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../admin_constants/admin_constants.dart';
import '../../preferences_manager/preferences_manager.dart';
import '../../resources/color_manager.dart';
import '../../resources/routes_manager.dart';
import '../cubit/admin_home_cubit.dart';
import '../cubit/admin_home_statese.dart';



class AdminDashboard extends StatelessWidget {
  const AdminDashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return BlocProvider(
      create: (context) {
        return AdminCubit()..getAllLeaders();
      },
      child: BlocConsumer<AdminCubit, AdminStates>(
        listener: (context, state) {
          // if(state is SuccessLogoutState){
          //   print('SuccessLogoutState');
          //   if(state.logoutModel.status==true){
          //     print('state.logoutModel.status==true');
          //     showToast(state.logoutModel.message??'success logout', ToastStates.Success);
          //     Navigator.pushReplacementNamed(context, Routes.adminLogin).then((value) {
          //       PreferencesManager.removeData('api_token');
          //       print('token is $token');
          //     }).catchError((error){
          //
          //       print(error.toString());
          //     });
          //
          //   }
          // }
          if(state is SuccessDeleteLeader){
            if(state.deleteLeaderModel.statusCode ==200){
              showToast(state.deleteLeaderModel.message??'delete', ToastStates.Success);
            }else{
              showToast(state.deleteLeaderModel.message??'error', ToastStates.Error);
            }
          }
        },
        builder: (context, state) {
          print('screen width is : $screenWidth');
          print('screen height is : $screenHeight');
          var cubit = AdminCubit.get(context);
          return Scaffold(
            body: Column(
              children: [
                if(screenHeight>122)
                Padding(
                  padding: const EdgeInsets.only(left: 10, top: 10),
                  child: (screenWidth >= 996 || (screenWidth < 996 &&cubit.folded )) ? Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Image.asset(
                          'assets/images/splash_logo.png', height: 70,),
                      ),
                      //  const SizedBox(width: 10,),
                      if(!(615<screenWidth&&screenWidth<649))
                       Text('Admin Dashboard', style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                          color: ColorManager.darkred
                       ),
                      ),
                      if(screenWidth>=561&&!(614<screenWidth&&screenWidth<694))
                      GestureDetector(
                        onTap: (){
                          showDialog(
                              context: context,
                              builder: (context) {
                                print('refreshhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh111111111111111111111111111111111111111');
                                return createLeaderDialog();
                                  },
                          );
                        },
                        child: Container(
                          margin: const EdgeInsets.only(left: 20),
                          width: 70,
                          height: 40  ,
                          decoration: BoxDecoration(
                            color: ColorManager.darkred,
                            borderRadius: BorderRadius.circular(10)
                          ),
                          child: const Center(
                              child: Text('create',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  fontSize: 17
                                ),
                              )
                          ),
                        ),
                      ),
                      const Spacer(),
                      searchBar(cubit),
                      if(screenWidth>=649)
                      const SizedBox(width: 10),
                      if(screenWidth>979  )
                        TextButton(onPressed: (){
                          PreferencesManager.removeData('api_token')
                              .then((value) {
                                if(value){
                                  Navigator.pushReplacementNamed(context, Routes.adminLogin);
                                  showToast('logout successfully', ToastStates.Success);
                                }
                          }).catchError((error){
                            showToast('something went wrong', ToastStates.Error);
                          });
                      }, child: Text('Logout',
                     //   textAlign: TextAlign.center,
                        style: TextStyle(
                            color: ColorManager.darkred,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,

                        ),
                        )
                      ),
                      //if(screenWidth>649 && screenWidth<614 )
                      const SizedBox(width: 10),
                    ],
                  ):
                  Row(
                    children: [
                      if(screenWidth>=612)
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Image.asset(
                          'assets/images/splash_logo.png', height: 70,),
                      ),
                      //  const SizedBox(width: 10,),
                      if(screenWidth>=815)
                        Text('Admin Dashboard', style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            color: ColorManager.darkred),
                        ),
                      if(screenWidth>=910)
                        GestureDetector(
                          onTap: (){
                            showDialog(
                              context: context,
                              builder: (context) {
                                print('refreshhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh111111111111111111111111111111111111111');
                                return createLeaderDialog();
                              },
                            );
                          },
                          child: Container(
                            margin: const EdgeInsets.only(left: 20),
                            width: 70,
                            height: 40  ,
                            decoration: BoxDecoration(
                                color: ColorManager.darkred,
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: const Center(
                                child: Text('create',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 17
                                  ),
                                )
                            ),
                          ),
                        ),
                      const Spacer(),
                      searchBar(cubit),
                      if(screenWidth>=999)
                      const SizedBox(width: 10),
                      if(screenWidth>=999)
                      TextButton(onPressed: (){

                      }, child: Text('Logout',
                        //   textAlign: TextAlign.center,
                        style: TextStyle(
                          color: ColorManager.darkred,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,

                        ),)
                      ),
                      if(screenWidth>=615)
                        const SizedBox(width: 10),
                    ],
                  )
                ),
                if(screenHeight>138)
                Container(
                  height: 2,
                  color: Colors.black38,
                  margin: const EdgeInsets.only(top: 12),
                ),
                if(state is LoadingDeleteLeader)
                LinearProgressIndicator(
                  color: ColorManager.darkred,
                ),
                if(screenHeight>144)
                Expanded(
                  child: Row(
                    children: [

                      if(screenHeight>144)
                      Container(
                        color: ColorManager.primary.withOpacity(0.05),
                        child: screenHeight>438? ClipPath(
                          clipper: WaveClipper(),
                          child: Container(
                            width: 230,
                              color: ColorManager.darkred,
                            //   color: ColorManager.primary.withOpacity(0.3),
                            // color: ColorManager.darkred,
                            // child:  Column(
                            //   children: [
                            //     SizedBox(
                            //       height: 10,
                            //     ),
                            //     Image.asset('images/fuller1.png',)
                            //   ],
                            // ),
                          ),
                        ):Container(),
                      ),
                      Container(
                        color: ColorManager.lightprimary ,
                      ),
                      Container(
                        width: 2,
                        height: double.infinity,
                        color: Colors.black38,
                        margin: const EdgeInsets.only(right: 20),
                      ),
                      if(screenHeight>170)
                      Expanded(
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          padding: const EdgeInsets.only(top: 15),
                          children: [
                            Column(
                              children: [
                                if(screenHeight>210)
                                Container(
                                  color: ColorManager.primary.withOpacity(0.1),
                                  height: 40,
                                  width: 2200,
                                  child: const Row(
                                    children: [
                                      SizedBox(width: 60,),
                                      Expanded(
                                        child: Text(style: TextStyle(
                                          color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold,),
                                            textAlign: TextAlign.center,
                                            'first name'),
                                      ),
                                      SizedBox(width:60,),
                                      Expanded(
                                        child: Text(style: TextStyle(
                                            color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
                                            textAlign: TextAlign.center,
                                            'last name'),
                                      ),
                                      SizedBox(width:60,),
                                      Expanded(
                                        child: Text(style: TextStyle(
                                            color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
                                            textAlign: TextAlign.center,
                                            'address'),
                                      ),
                                      SizedBox(width:60,),
                                      Expanded(
                                        child: Text(style: TextStyle(
                                            color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
                                            textAlign: TextAlign.center,
                                            'guardian'),
                                      ),
                                      SizedBox(width:60),
                                      Expanded(
                                        child: Text(style: TextStyle(
                                            color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
                                            textAlign: TextAlign.center,
                                            'phone'),
                                      ),
                                      SizedBox(width:60 ,),
                                      Expanded(
                                        child: Text(style: TextStyle(
                                            color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
                                            textAlign: TextAlign.center,
                                            'land line'),
                                      ),
                                      SizedBox(width:60,),
                                      Expanded(
                                        child: Text(style: TextStyle(
                                            color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
                                            textAlign: TextAlign.center,
                                            'hoobbies'),
                                      ),
                                      SizedBox(width:60,),
                                      Expanded(
                                        child: Text(style: TextStyle(
                                            color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
                                            textAlign: TextAlign.center,
                                            'branch'),
                                      ),
                                      SizedBox(width:60,),
                                      Expanded(
                                        child: Text(style: TextStyle(
                                            color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
                                            textAlign: TextAlign.center,
                                            'librarian'),
                                      ),
                                      SizedBox(width:20,),
                                      Expanded(
                                        child: Text('delete',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Expanded(
                                  child: SizedBox(
                                    width: 2200,
                                    child: ListView.separated(
                                        scrollDirection: Axis.vertical,
                                        itemBuilder: (context, index) =>
                                            leadersInfoList(index,cubit,state),
                                        separatorBuilder: (context, index) =>
                                            const SizedBox(height: 20),
                                        itemCount: cubit.getAllLeadersModel!=null?(cubit.getAllLeadersModel!.data!=null?(cubit.getAllLeadersModel!.data!.length):0):0
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      //  GridView(gridDelegat)
                    ],
                  ),
                ),
                const SizedBox(
                  height: 40,
                )
              ],
            ),
          );
        },
      ),
    );
  }

  Widget searchBar(AdminCubit cubit) {
    return AnimatedContainer(
      curve: Curves.fastOutSlowIn,
      duration: const Duration(seconds: 1),
      height: 50,
      width: cubit.folded ? 155 : 490,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(300.0),
        color: ColorManager.primary.withOpacity(0.2),
        boxShadow: kElevationToShadow[0.9],
      ),
      child: cubit.folded ? GestureDetector(
        child: const Padding(
          padding: EdgeInsets.symmetric(horizontal: 18.0),
          child: Row(
            children: [
              Center(
                  child: Text('Search...',
                  style: TextStyle(fontSize: 20, color: Colors.black54,fontWeight: FontWeight.bold),
                  textAlign: TextAlign.end)
              ),
              Spacer(),
              Icon(
                Icons.search,
                color: Colors.black54,
              ),
            ],
          ),
        ),
        onTap: () {
          cubit.expandSearchBar();
          if(cubit.searchController.text.isEmpty) {
            Timer(const Duration(seconds: 5), cubit.resetSearchBar);
          }
        },
      )
          : TextFormField(
          onChanged: (text) {
            if (text.isEmpty) {
              Timer(const Duration(seconds: 5), cubit.resetSearchBar);
              cubit.getAllLeaders();
            }
            else{
              cubit.getAllLeaders(text: text);
            }
          },
          controller: cubit.searchController,
          decoration: const InputDecoration(
            hintText: 'Search....',
            hintStyle: TextStyle(
              color: Colors.black54,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
            suffixIcon: Icon(Icons.search,color: Colors.black54),
            contentPadding: EdgeInsets.symmetric(
                vertical: 10.0, horizontal: 10.0),
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
          ),
          cursorColor: ColorManager.primary,
          style: const TextStyle(
              fontSize: 20,
             color: Colors.black87
          ),
          cursorHeight: 20
      ),
    );
  }



  Widget leadersInfoList(int index,AdminCubit cubit,state) {
    // if (index == 0) {
    //   return Container(
    //     color: ColorManager.primary.withOpacity(0.1),
    //     child: const Row(
    //       children: [
    //         SizedBox(width: 60,),
    //         Expanded(
    //           child: Text(style: TextStyle(
    //               color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold,),
    //               textAlign: TextAlign.center,
    //               'first name'),
    //         ),
    //         SizedBox(width:60,),
    //         Expanded(
    //           child: Text(style: TextStyle(
    //               color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
    //               textAlign: TextAlign.center,
    //               'last name'),
    //         ),
    //         SizedBox(width:60,),
    //         Expanded(
    //           child: Text(style: TextStyle(
    //               color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
    //               textAlign: TextAlign.center,
    //               'address'),
    //         ),
    //         SizedBox(width:60,),
    //         Expanded(
    //           child: Text(style: TextStyle(
    //               color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
    //               textAlign: TextAlign.center,
    //               'guardian'),
    //         ),
    //         SizedBox(width:60),
    //         Expanded(
    //           child: Text(style: TextStyle(
    //               color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
    //               textAlign: TextAlign.center,
    //               'phone'),
    //         ),
    //         SizedBox(width:60 ,),
    //         Expanded(
    //           child: Text(style: TextStyle(
    //               color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
    //               textAlign: TextAlign.center,
    //               'land line'),
    //         ),
    //         SizedBox(width:60,),
    //         Expanded(
    //           child: Text(style: TextStyle(
    //               color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
    //               textAlign: TextAlign.center,
    //               'hoobbies'),
    //         ),
    //         SizedBox(width:60,),
    //         Expanded(
    //           child: Text(style: TextStyle(
    //               color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
    //               textAlign: TextAlign.center,
    //               'branch'),
    //         ),
    //         SizedBox(width:60,),
    //         Expanded(
    //           child: Text(style: TextStyle(
    //               color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
    //               textAlign: TextAlign.center,
    //               'librarian'),
    //         ),
    //         SizedBox(width:20,),
    //         Expanded(
    //           child: Text('delete',
    //               textAlign: TextAlign.center,
    //               style: TextStyle(
    //               color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold,
    //             ),
    //           ),
    //         ),
    //       ],
    //     ),
    //   );
    // }
    return Container(
      color: ColorManager.primary.withOpacity(0.1),
      child: Row(
        children: [
          const SizedBox(width:60),
           Expanded(
            child: Text(style: const TextStyle(color: Colors.black87, fontSize: 18,),
                textAlign: TextAlign.center,
                '${cubit.getAllLeadersModel!.data![index].firstName} '),
          ),
          const SizedBox(width: 60,),
           Expanded(
            child: Text(style: const TextStyle(color: Colors.black87, fontSize: 18,),
                textAlign: TextAlign.center,
                '${cubit.getAllLeadersModel!.data![index].lastName} '),
          ),
          const SizedBox(width: 60,),
           Expanded(
            child: Text(style: const TextStyle(color: Colors.black87, fontSize: 18,),
                textAlign: TextAlign.center,
                '${cubit.getAllLeadersModel!.data![index].address} '),
          ),
          const SizedBox(width: 60,),
           Expanded(
            child: Text(style: const TextStyle(color: Colors.black87, fontSize: 18,),
                textAlign: TextAlign.center,
                '${cubit.getAllLeadersModel!.data![index].guardian} '),
          ),
          const SizedBox(width: 60,),
           Expanded(
            child: Text(style: const TextStyle(color: Colors.black87, fontSize: 18,),
                textAlign: TextAlign.center,
                '${cubit.getAllLeadersModel!.data![index].phone} '),
          ),
          const SizedBox(width: 60,),
           Expanded(
            child: Text(style: const TextStyle(color: Colors.black87, fontSize: 18,),
                textAlign: TextAlign.center,
                '${cubit.getAllLeadersModel!.data![index].landLineTelephone} '),
          ),
       const SizedBox(width: 60,),
           Expanded(
            child: Text(style: const TextStyle(color: Colors.black87, fontSize: 18,),
                textAlign: TextAlign.center,
                '${cubit.getAllLeadersModel!.data![index].hobbies} '),
          ),
          const SizedBox(width: 60,),
           Expanded(
            child: Text(style: const TextStyle(color: Colors.black87, fontSize: 18,),
                textAlign: TextAlign.center,
                '${cubit.getAllLeadersModel!.data![index].branch!.map((e) => e.name)}'),
          ),
          const SizedBox(width: 60,),
           Expanded(
            child: Text(style: const TextStyle(color: Colors.black87, fontSize: 18,),
                textAlign: TextAlign.center,
                '${cubit.getAllLeadersModel!.data![index].librarian}'),
          ),
          const SizedBox(width: 20,),
          Expanded(
            child:   TextButton(onPressed: (){
              print(index);
              cubit.deleteLeader((cubit.getAllLeadersModel!.data![index].id)!);
            }, child: const Icon(Icons.delete,color: Colors.black54,)
            )
            ),

        ],
      ),
    );
  }

  Widget createLeaderDialog() {
    return BlocProvider(
      create: (context) => AdminCubit(),
      child: BlocConsumer<AdminCubit,AdminStates>(
        listener: (context, state) {

        },
        builder: (context, state) {
          var cubit = AdminCubit.get(context);
          return Stack(
            children: [
              RotationTransition(
                  turns:  const AlwaysStoppedAnimation(-15 / 360),
                  child: AlertDialog(
                    scrollable: true,
                    backgroundColor: ColorManager.lightprimary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      // side: BorderSide(
                      //   color: Colors.yellow
                      // )
                    ),
                    content: const SizedBox(
                      width: 700,
                      height: 400,
                    ),
                  )
              ),
              AlertDialog(
                // clipBehavior: Clip.antiAlias,
                backgroundColor: ColorManager.lightGrey,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                  // side: BorderSide(
                  //   color: Colors.yellow
                  // )
                ),
                actions:const [
                  // Container(
                  //   margin: EdgeInsets.only(left: 20),
                  //   width: 70,
                  //   height: 40  ,
                  //   decoration: BoxDecoration(
                  //       color: ColorManager.darkred,
                  //       borderRadius: BorderRadius.circular(10)
                  //   ),
                  //   child: Center(
                  //       child: Text('create',
                  //         style: TextStyle(
                  //             color: Colors.white,
                  //             fontWeight: FontWeight.bold,
                  //             fontSize: 17
                  //         ),
                  //       )
                  //   ),
                  // ),
                ],
                scrollable: true,
                content: SizedBox(
                  width: 700,
                  height: 400,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Create leader',
                        style: TextStyle(
                            fontSize: 40,
                            color: ColorManager.darkred
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        const Text('First name',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600
                                            )
                                        ),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(5),
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                                fontSize: 15,
                                                color: Colors.black54

                                            ),
                                            decoration: const InputDecoration(
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none,
                                            ),
                                            controller: cubit.firstNameController,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        const Text('Last name',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(5),
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                                fontSize: 15,
                                                color: Colors.black54

                                            ),
                                            controller: cubit.lastNameController,
                                            decoration: const InputDecoration(
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),

                              const Text('Username',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w600
                                  )
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: TextFormField(
                                  style: const TextStyle(
                                      fontSize: 15,
                                      color: Colors.black54

                                  ),
                                  controller: cubit.userNameController,
                                  decoration: const InputDecoration(
                                    enabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        const Text('Password',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600
                                            )
                                        ),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(5),
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                                fontSize: 15,
                                                color: Colors.black54
                                            ),
                                            controller: cubit.passwordController,
                                            decoration: const InputDecoration(
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        const Text('Password Confirmation',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(5),
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                                fontSize: 15,
                                                color: Colors.black54
                                            ),
                                            controller: cubit.passwordConfirmationController,
                                            decoration: const InputDecoration(
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),

                              const Text('Guardian name',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w600
                                  )
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: TextFormField(
                                  style: const TextStyle(
                                      fontSize: 15,
                                      color: Colors.black54

                                  ),
                                  controller: cubit.guardianController,
                                  decoration: const InputDecoration(
                                    enabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const Text('Phone',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w600
                                  )
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: TextFormField(
                                  style: const TextStyle(
                                      fontSize: 15,
                                      color: Colors.black54
                                  ),
                                  controller: cubit.phoneController,
                                  decoration: const InputDecoration(
                                    enabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        const Text('Address',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.w600
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(5),
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                                fontSize: 15,
                                                color: Colors.black54
                                            ),
                                            controller: cubit.addressController,
                                            decoration: const InputDecoration(
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        const Text('Land line',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(5),
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                                fontSize: 15,
                                                color: Colors.black54

                                            ),
                                            controller: cubit.landLineController,
                                            decoration: const InputDecoration(
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const Text('hobbies',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w600
                                  )
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: TextFormField(
                                  style: const TextStyle(
                                      fontSize: 15,
                                      color: Colors.black54

                                  ),
                                  controller: cubit.hobbiesController,
                                  decoration: const InputDecoration(
                                    enabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                  ),
                                ),
                              ),

                              const SizedBox(
                                height: 10,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text('Branch',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600
                                      )
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                      width: double.infinity,
                                      height: 50,
                                      padding: const EdgeInsets.only(left: 10,right: 10),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      child:  Container(
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(5),
                                        ),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            TextButton(
                                              child: Text('Flowers',
                                                style: TextStyle(
                                                    fontSize: 17,
                                                  fontWeight: FontWeight.bold,
                                                  color: cubit.selectFlowers?ColorManager.darkred:Colors.grey
                                                ),),
                                              onPressed: (){
                                                cubit.selectBranch1('Flowers');
                                              },
                                            ),
                                            TextButton(
                                              child: Text('Cubs',
                                                style: TextStyle(
                                                    fontSize: 17,
                                                  fontWeight: FontWeight.bold,
                                                  color: cubit.selectCubs?ColorManager.darkred:Colors.grey
                                                ),),
                                              onPressed: (){
                                                cubit.selectBranch1('Cubs');
                                              },
                                            ),
                                            TextButton(
                                              child: Text('Rangers',
                                                style: TextStyle(
                                                    fontSize: 17,
                                                  fontWeight: FontWeight.bold,
                                                  color: cubit.selectRangers?ColorManager.darkred:Colors.grey
                                                ),),
                                              onPressed: (){
                                                cubit.selectBranch1('Rangers');
                                              },
                                            ),
                                            TextButton(
                                              child: Text('Advanced',
                                                style: TextStyle(
                                                    fontSize: 17,
                                                  fontWeight: FontWeight.bold,
                                                  color: cubit.selectAdvanced?ColorManager.darkred:Colors.grey
                                                ),),
                                              onPressed: (){
                                                cubit.selectBranch1('Advanced');
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    // child: DropdownButton<String>(
                                    //   // Step 3.
                                    //   value: cubit.dropdownValue,
                                    //   // Step 4.
                                    //   items: cubit.items
                                    //       .map<DropdownMenuItem<String>>((String value) {
                                    //
                                    //     return DropdownMenuItem<String>(
                                    //       value: value,
                                    //       child: Text(
                                    //         value,
                                    //         style: const TextStyle(fontSize: 20,color: Colors.black54),
                                    //       ),
                                    //     );
                                    //   }).toList(),
                                    //   // Step 5.
                                    //   onChanged: (String? newBranch) {
                                    //
                                    //     cubit.selectBranch(newBranch!);
                                    //   },
                                    // ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment : CrossAxisAlignment.start,
                                      children: [
                                        const Text('Birth date',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600
                                            )
                                        ),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(5),
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                                fontSize: 15,
                                                color: Colors.black54
                                            ),
                                            controller: cubit.dateController,
                                            readOnly: true,
                                            onTap: (){
                                              cubit.pickDate(context);
                                            },
                                            decoration: const InputDecoration(
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none,
                                              suffixIcon:Icon(Icons.date_range_outlined),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  ),

                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        const Text('Librarian',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          width: double.infinity,
                                          padding: const EdgeInsets.only(left: 10,right: 10),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(5),
                                          ),
                                          child: DropdownButton<bool>(
                                            // Step 3.
                                            value: cubit.isLibrarian,
                                            // Step 4.
                                            items: cubit.librarians
                                                .map<DropdownMenuItem<bool>>((bool value) {
                                              return DropdownMenuItem<bool>(
                                                value: value,
                                                child: Text(
                                                  '$value',
                                                  style: const TextStyle(fontSize: 20,color: Colors.black54),
                                                ),
                                              );
                                            }).toList(),
                                            // Step 5.
                                            onChanged: (bool ? newValue) {
                                              cubit.giveLibrarianProperty(newValue!);
                                              print('newValue isssssssssss : $newValue') ;
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 10),
                              // TextButton(
                              //     onPressed: (){
                              //       //cubit.getProfileImage();
                              //     },
                              //     child:cubit.profileImage!=null?
                              //     Text('image has been selected press again to change',style: TextStyle(color: ColorManager.darkred,fontWeight: FontWeight.bold,fontSize: 18),):
                              //     Text('add photo',style: TextStyle(color: ColorManager.darkred,fontWeight: FontWeight.bold,fontSize: 18),)
                              // ),

                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  TextButton(
                                    onPressed: (){
                                    Navigator.pop(context);
                                  }, child: Text('Cancel',
                                    style: TextStyle(
                                        color: ColorManager.lightprimary,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                    ),
                                   ),
                                  ),
                                  GestureDetector(
                                    onTap: (){
                                      cubit.pickFile();
                                      // cubit.uploadDataImage(
                                      //     firstName: cubit.firstNameController.text,
                                      //     lastName: cubit.lastNameController.text,
                                      //     userName: cubit.userNameController.text,
                                      //     password: cubit.passwordController.text,
                                      //     passwordConfirmation: cubit.passwordConfirmationController.text,
                                      //     phone: cubit.phoneController.text,
                                      //     address: cubit.addressController.text,
                                      //     guardian: cubit.guardianController.text,
                                      //     landLineTelephone: cubit.landLineController.text,
                                      //     file: cubit.profileImage??cubit.emptyFile,
                                      //     hobbies: cubit.hobbiesController.text,
                                      //     librarian: cubit.isLibrarian,
                                      //     birthdate: cubit.dateController.text,
                                      //     branch: cubit.branches
                                      // );
                                    },

                                    child: state is !LoadingCreateLeader? Container(
                                      margin: const EdgeInsets.only(left: 20),
                                      width: 70,
                                      height: 40,
                                      decoration: BoxDecoration(
                                        color: ColorManager.darkred,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      child: const Center(
                                        child: Text('create',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17
                                          ),
                                        ),
                                      ),
                                    ): const Center(child: CircularProgressIndicator()),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}


class WaveClipper extends CustomClipper<Path>{
  @override
  Path getClip(Size size) {

    var path = new Path();
    double width = size.width;
    double height = size.height;
    path.moveTo(width,size.height*0.5 );
    path.lineTo(width, size.height); //start path with this if you are making at bottom
    path.quadraticBezierTo( width*0.5, height*0.65,0, height*0.8);

    path.moveTo(0,size.height*0.5 );
    path.lineTo(0, size.height); //start path with this if you are making at bottom
    //path.lineTo(width*0.5, size.height); //start path with this if you are making at bottom
    path.quadraticBezierTo( width*0.5, height*0.65,width, height*0.8);


    path.moveTo(width,size.height*0.5-303 );
    path.lineTo(width, size.height-303);
    path.quadraticBezierTo( width*0.5, height*0.65-303,0, height*0.8-303);


    path.moveTo(0,size.height*0.5 -303);
    path.lineTo(0, size.height-303); //start path with this if you are making at bottom
    //path.lineTo(width*0.5, size.height); //start path with this if you are making at bottom
    path.quadraticBezierTo( width*0.5, height*0.65-303,width, height*0.8-303);

    path.moveTo(width,size.height*0.5 );
    path.lineTo(width, 0);
    path.quadraticBezierTo( width*0.5, height*0.35,0, height*0.2);

    path.moveTo(0,size.height*0.5 );
    path.lineTo(0, 0);
    path.quadraticBezierTo( width*0.5, height*0.35,width, height*0.2);
    path.moveTo(width,size.height*0.5 +303);
    path.lineTo(width, 0+303);
    path.quadraticBezierTo( width*0.5, height*0.35+303,0, height*0.2+303);

    path.moveTo(0,size.height*0.5+303 );
    path.lineTo(0, 0+303);
    path.quadraticBezierTo( width*0.5, height*0.35+303,width, height*0.2+303);


    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false; //if new instance have different instance than old instance
    //then you must return true;
  }
}

