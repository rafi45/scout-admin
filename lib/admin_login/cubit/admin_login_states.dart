
import '../model/admin_login_model.dart';

abstract class AdminLoginStates{}

class InitialAdminLoginState   extends AdminLoginStates{}


class LoadingAdminLoginState   extends AdminLoginStates{}
class SuccessAdminLoginState   extends AdminLoginStates{

  AdminLoginModel adminLoginModel;
  SuccessAdminLoginState(this.adminLoginModel);
}
class ErrorAdminLoginState   extends AdminLoginStates{}


class ChangeVisibility   extends AdminLoginStates{}