import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../http_constants.dart';
import '../../../http_manager.dart';
import '../model/admin_login_model.dart';
import 'admin_login_states.dart';

class AdminLoginCubit extends Cubit<AdminLoginStates>{
  AdminLoginCubit() : super(InitialAdminLoginState());

  static AdminLoginCubit get(context){
    return BlocProvider.of(context);
  }

  bool notSeen = true;

  void makePasswordVisible(){
    notSeen = !notSeen;
    emit(ChangeVisibility());
    print(notSeen);
  }


  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();


  late AdminLoginModel adminLoginModel;
  Future adminLogin(String userName, String password) async{
    emit(LoadingAdminLoginState());
    return await Http.postData(
        path: adminLoginPath,
        map:{
          'username' : userName,
          'password': password,
        },
    ).then((value) {
      print(value.statusCode);
      //print(value.body);
      print('valuevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevalue');
      print(jsonDecode(value.body));
      adminLoginModel = AdminLoginModel.fromJson(jsonDecode(value.body));
      emit(SuccessAdminLoginState(adminLoginModel));
    }).catchError((error){
      print('errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr');
      print(error.toString());
      emit(ErrorAdminLoginState());
    });
  }

}