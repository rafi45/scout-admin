
class AdminLoginModel{

  Data? data;
  String message ;
  int statusCode;

  AdminLoginModel({
    this.data,
    required this.message ,
    required this.statusCode
  });

  factory  AdminLoginModel.fromJson(Map<String, dynamic> json) {
    return AdminLoginModel(
        message: json['message'] ,
        statusCode:  json['status code'],
        data : json['data'] != null ?  Data.fromJson(json['data']) : null
    );
  }

}

class Data{
  int  id;
  String firstName;
  String lastName;
  String phone;
  String username;
  int roleId;
  String apiToken;
  Data({
        required this.id,
        required this.firstName,
        required this.lastName,
        required this.phone,
        required this.username,
        required this.roleId,
        required this.apiToken,
  });

  factory Data.fromJson(Map<String,dynamic>json){
    return Data(
        id: json['id'],
        firstName: json['first_name'],
        lastName: json['last_name'],
        phone: json['phone'],
        username: json['username'],
        roleId: json['role_id'],

        apiToken: json['api_token'],
    );
  }
}

