import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../admin_constants/admin_constants.dart';
import '../../preferences_manager/preferences_manager.dart';
import '../../resources/color_manager.dart';
import '../../resources/routes_manager.dart';
import '../cubit/admin_login_cubit.dart';
import '../cubit/admin_login_states.dart';

class AdminLogin extends StatelessWidget {
  const AdminLogin({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    GlobalKey<FormState> key = GlobalKey<FormState>();
    return  Scaffold(
      // body: Container(
      //   padding: const EdgeInsets.only(top: 120),
      // decoration: BoxDecoration(
      //   gradient: LinearGradient(
      //     begin: Alignment.bottomRight,
      //     end: Alignment.topLeft,
      //     colors: [ColorManager.darkblue,ColorManager.darkred,ColorManager.lightprimary,],
      //   ),
      // ),
      // child: Center(
      //   child: Stack(
      //     alignment: Alignment.center,
      //     children: [
      //       Container(
      //         height:500,
      //         width :350,
      //         decoration: BoxDecoration(
      //           borderRadius: BorderRadius.circular(20),
      //           color: ColorManager.gray,
      //         ),
      //         child: Column(
      //           mainAxisAlignment: MainAxisAlignment.center,
      //           children: [
      //             Padding(
      //               padding: const EdgeInsets.symmetric(horizontal: 8.0),
      //               child: TextFormField(
      //                 decoration: const InputDecoration(
      //                   border: InputBorder.none,
      //                   hintText: 'email',
      //                   prefixIcon: Icon(Icons.email),
      //                 ),
      //               ),
      //             ),
      //             const SizedBox(
      //               height: 10,
      //             ),
      //             Padding(
      //               padding: const EdgeInsets.symmetric(horizontal: 8.0),
      //               child: TextFormField(
      //                 decoration: const InputDecoration(
      //                   border: InputBorder.none,
      //                   hintText: 'password',
      //                   prefixIcon: Icon(Icons.password),
      //                 ),
      //               ),
      //             ),
      //             const SizedBox(
      //               height: 60,
      //             ),
      //             Padding(
      //               padding: const EdgeInsets.symmetric(horizontal: 14.0),
      //               child: ButtonWidget(
      //                   btnText: 'Login',
      //               onClick: (){
      //                     Navigator.pushReplacementNamed(context, Routes.adminRoute);
      //               }),
      //             ),
      //           ],
      //         ),
      //       ),
      //       Align(
      //         alignment: Alignment.topCenter,
      //         child: Image.asset('assets/images/splash_logo.png',
      //         height: 125,
      //         ),
      //       ),
      //     ],
      //   ),
      //  ),
      // ),
      body: BlocProvider(
        create: (context) => AdminLoginCubit(),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Stack(
              children: [
                Container(
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                      image: AssetImage('images/scouts/fire2.jpg',),
                      fit: BoxFit.cover
                    ),
                    //boxShadow: [BoxShadow(color: Colors.black),BoxShadow(color: Colors.black),BoxShadow(color: Colors.black),]
                  ),

                ),
                Container(
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [Colors.black87,Colors.black54],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter
                    )
                  ),
                )
              ],
            ),
            Container(
              width: 600,
              height: 400,
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(15),
              ),
              child: Center(
                child: SingleChildScrollView(
                  child: BlocConsumer<AdminLoginCubit,AdminLoginStates>(
                    listener: (context, state) {
                      if(state is SuccessAdminLoginState){
                        if(state.adminLoginModel.statusCode==200){
                          showToast(
                              state.adminLoginModel.message,
                              ToastStates.Success
                          );
                          PreferencesManager.setData(
                              key: 'api_token',
                              value:state.adminLoginModel.data!.apiToken,
                          ).then((value) {
                            token = state.adminLoginModel.data!.apiToken;
                            print('token is : $token');
                            Timer(const Duration(seconds: 1), () {
                              Navigator.pushReplacementNamed(context, Routes.adminRoute);
                            });
                          }).catchError((error){
                            print(error.toString());
                          });
                        }else{
                          print(state.adminLoginModel.message);
                          showToast(
                              state.adminLoginModel.message,
                              ToastStates.Error,
                          );
                        }
                      }
                    },
                    builder: (context, state) {
                      var cubit = AdminLoginCubit.get(context);
                      return Form(
                        key: key,
                        child: Column(
                          //  mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text('Scout Management',style: TextStyle(fontWeight: FontWeight.bold,fontSize:30,color: ColorManager.primary)),
                            const SizedBox(height: 20),
                            Container(

                              decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(0.2),
                                  borderRadius: BorderRadius.circular(10),
                              ),
                              child: TextFormField(
                                decoration: InputDecoration(
                                  enabledBorder: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  focusedErrorBorder: InputBorder.none,
                                  border: InputBorder.none,
                                  errorStyle: TextStyle(
                                    color: ColorManager.lightprimary,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold
                                  ),
                                  hintText: 'user name',
                                  hintStyle: const TextStyle(
                                      color: Colors.white70,
                                      fontSize: 20,
                                  ),
                                  prefixIcon: Icon(Icons.email_outlined,color: ColorManager.primary,),
                                ),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter some text';
                                  }else if(value.length<6){
                                    return 'username must be at least 6 characters';
                                  }
                                  return null;
                                },
                                controller: cubit.userNameController ,
                                cursorColor: Colors.grey,
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                ),
                              ),
                            ),
                            const SizedBox(height: 15),
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(0.2),
                                  borderRadius: BorderRadius.circular(10)
                              ),
                              child: TextFormField(
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    errorBorder: InputBorder.none,
                                    focusedErrorBorder: InputBorder.none,
                                    errorStyle: TextStyle(
                                      color: ColorManager.lightprimary,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold
                                    ),
                                    hintText: 'password',
                                    hintStyle: const TextStyle(
                                        color: Colors.white70,
                                        fontSize: 20,
                                    ),
                                    prefixIcon: Icon(Icons.lock,color: ColorManager.primary,),
                                    suffixIcon:cubit.notSeen ?
                                    IconButton(
                                      icon: Icon(Icons.visibility_off_outlined,color: ColorManager.primary),
                                      onPressed: (){
                                        cubit.makePasswordVisible();
                                      },
                                    ):
                                    IconButton(
                                      icon : Icon(Icons.visibility,color: ColorManager.primary,),
                                      onPressed: (){
                                        cubit.makePasswordVisible();
                                      },
                                    )
                                ),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter some text';
                                  }else if(value.length<6){
                                    return 'password must be at least 6 characters';
                                  }
                                  return null;
                                },
                                controller: cubit.passwordController,
                                obscureText: cubit.notSeen,
                                cursorColor: Colors.grey,
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 25,
                            ),
                            state is !LoadingAdminLoginState?
                            GestureDetector(
                              onTap: (){
                                if (key.currentState!.validate()) {
                                  cubit.adminLogin(
                                      cubit.userNameController.text,
                                      cubit.passwordController.text,
                                  );
                                }
                              },
                              child: Container(
                                height: 40,
                                width: 230,
                                padding: const EdgeInsets.only(top: 7),
                                decoration: BoxDecoration(
                                  color: ColorManager.lightprimary,
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                child: const Text('Login',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ): Center(
                                child:  CircularProgressIndicator(
                                  color: ColorManager.lightprimary,
                                )
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}