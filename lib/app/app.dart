import 'package:flutter/material.dart';


import '../resources/routes_manager.dart';
import '../resources/them_manager.dart';
class MyApp extends StatefulWidget {
 // const MyApp({Key? key}) : super(key: key); //defult constry
MyApp._internal () ;
static final MyApp _instence = MyApp._internal () ;
factory MyApp() => _instence ;
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      onGenerateRoute: RouteGenerator.getRoute,
      initialRoute: Routes.adminLogin,
      theme: getapptheme(),
    ) ;
  }
}
