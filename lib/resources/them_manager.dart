import 'package:admin/resources/stayle_manager.dart';
import 'package:flutter/material.dart';

import 'color_manager.dart';
import 'font_manager.dart';



ThemeData getapptheme() {
  return ThemeData(
//maincolor
      primaryColor: ColorManager.darkred,
      primaryColorDark: ColorManager.darkgray,
      splashColor: ColorManager.lightprimary,
//cardview
      cardTheme: CardTheme(
        color: ColorManager.white,
        shadowColor: ColorManager.darkgray,
        elevation: 4,
      ),
      //appbar
      appBarTheme: AppBarTheme(
          centerTitle: true,
          color: ColorManager.darkred,
          shadowColor: ColorManager.lightprimary,
          elevation:5,
         // titleTextStyle: getregulerstyle(FontSize.v16, ColorManager.white)
      ),
      //bottom
      buttonTheme: ButtonThemeData(
          shape: const StadiumBorder(),
          buttonColor: ColorManager.primary,
          splashColor: ColorManager.lightprimary),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          primary: ColorManager.primary,
          textStyle: getregulerstyle(FontSize.v16, ColorManager.white),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12)),
        ),
      ),
      //text
      textTheme: TextTheme(
          displayLarge: getlightstyle(FontSize.v23, ColorManager.white),
          headlineLarge: getsemiboldstyle(FontSize.v16, ColorManager.darkgray),
          titleMedium: getmediumstyle(FontSize.v14, ColorManager.lightprimary)),
      //textfromfiled
      inputDecorationTheme: InputDecorationTheme(
//content
          contentPadding: const EdgeInsets.all(FontSize.v12),
//hint
          hintStyle: getregulerstyle(FontSize.v14, ColorManager.darkgray),
          labelStyle: getmediumstyle(FontSize.v14, ColorManager.darkgray),
          errorStyle: getregulerstyle(FontSize.v14, ColorManager.error),
//enableborder
          enabledBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: ColorManager.primary, width: FontSize.v1_5),
              borderRadius: BorderRadius.all(Radius.circular(FontSize.v8))),
          //foucs
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: ColorManager.darkblue, width: FontSize.v1_5),
              borderRadius: BorderRadius.all(Radius.circular(FontSize.v8))),
          //error
          errorBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: ColorManager.error, width: FontSize.v1_5),
              borderRadius: BorderRadius.all(Radius.circular(FontSize.v8)) //
              ),
              //error تزبيط
          focusedErrorBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: ColorManager.primary, width: FontSize.v1_5),
              borderRadius: BorderRadius.all(Radius.circular(FontSize.v8)))));
}
