import 'package:flutter/material.dart';

class FontConstant {

 static const String fontfamily = "Montserrat" ;
}

class FontWightManager {
  static const FontWeight reguler = FontWeight.w400 ;
 static const FontWeight light = FontWeight.w300 ;
static const FontWeight bold = FontWeight.w700 ;
static const FontWeight semibold = FontWeight.w600 ;
static const FontWeight medium = FontWeight.w500 ;
}
class FontSize {
static const double v1_5 = 1.5 ;
  static const double v8 = 8.0 ;
static const double v12 = 12.0 ;
static const double v14 = 14.0 ;
static const double v16 = 16.0 ;
static const double v23 = 23.0 ;
}