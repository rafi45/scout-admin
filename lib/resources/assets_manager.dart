const String mainPath = "assets/images";
const String secondPath = "assets/images/scouts";

class ImageAssets {
  static String splashlogo = "$mainPath/splash_logo.png";
  static String background = "$mainPath/background1.jpg";
  static String advancedlogo = "$mainPath/Advanced branch.jpg";
  static String flowerslogo = "$mainPath/flower.jpg";
  static String cubslogo = "$mainPath/LionCub.png";
  static String travelslogo = "$mainPath/travels.jpg";
  static String pranchpackground = "$mainPath/pranch.jpg";
  static String test="$mainPath/LionCubs.png";
  static String cubsBranch="$secondPath/boysScouts.jpg";
  static String flowersBranch="$secondPath/childScout.jpg";
  static String rangersBranch="$secondPath/youngScoutsFlag.jpg";
  static String advancedBranch="$secondPath/maleScout.jpg";
  static String camping2="$secondPath/camping2.jpg";
  static String activities="$secondPath/activites.jpg";
  static String group="$secondPath/groupScout.jpg";
  static String vanguard="$secondPath/vanguard.jpg";
  static String profileAppBar="$secondPath/maleScout2.jpg";

}
