import 'package:flutter/material.dart';

import 'font_manager.dart';

TextStyle _gettextstyle(double fontsize, FontWeight fontweight, Color color) {
  return TextStyle(
    fontSize: fontsize,
    fontFamily: FontConstant.fontfamily,
    color: color,
    fontWeight: fontweight,
  );
}


TextStyle getregulerstyle(double fontsize, Color color) {
  return _gettextstyle(fontsize = FontSize.v12, FontWightManager.reguler, color);
}
TextStyle getboldstyle(double fontsize, Color color) {
  return _gettextstyle(fontsize = FontSize.v12, FontWightManager.bold, color);
}

TextStyle getlightstyle(double fontsize,  Color color) {
  return _gettextstyle(fontsize = FontSize.v12, FontWightManager.light, color);
}

TextStyle getmediumstyle(double fontsize, Color color) {
  return _gettextstyle(fontsize = FontSize.v12, FontWightManager.medium, color);
}

TextStyle getsemiboldstyle(
    double fontsize,  Color color) {
  return _gettextstyle(
      fontsize = FontSize.v12, FontWightManager.semibold, color);
}
