import 'package:flutter/material.dart';

import '../admin_home/view/admin_home.dart';
import '../admin_login/view/admin_login.dart';
import '../upload_files.dart';


class Routes{
static const String splashRoute = "/";
static const String loginRoute = "/login";
static const String mainRoute = "/main";
static const String branshRoute = "/bransh";
static const String eventRoute = "/event";
static const String meetingRoute = "/meeting";
static const String cmeetingRoute = "/createmeeting";
static const String membersRoute = "/membar";
static const String vworkRoute= "/vwork";
static const String campRoute= "/createcamp";
static const String vanguardsDetailsRoute="/vanguards_details";
static const String membersDetailsRout="/all_members";
static const String createNewMemberRoute="/create_new_member";
static const String createNewVanguardRoute='/create_new_vanguard';
static const String adminRoute='/admin';
static const String adminLogin='/admin_login';
static const String uploadFile='/upload_file';


static const String viewMemberDetailsRoute="/view_member_details";
static const String pickingVanguardMembersRoute="/picking_members";
static const String homePageRoute='/home_page';
static const String userHomePageRoute='/userHomePage';
static const String userProfileRoute='/user_profile';
static const String meetingsPageRoute='/meetings_page';
static const String userEventsPageRoute='/user_event_page';
static const String userCharityPageRoute='/charity_page';
static const String userTripPageRoute='/user_trip_page';

 }


class RouteGenerator {
  static Route<dynamic> getRoute (RouteSettings settings)
  { switch (settings.name){

    case Routes.adminLogin:
      return MaterialPageRoute(builder: (_)=>  const AdminLogin());
    case Routes.adminRoute:
      return MaterialPageRoute(builder: (_)=>  const AdminDashboard());
    case Routes.uploadFile:
      return MaterialPageRoute(builder: (_)=>  const UploadFiles());

  default:
return undefinedroute ();

}
}

static Route<dynamic> undefinedroute (){
  return MaterialPageRoute(builder: (_)=>Scaffold(appBar: AppBar(title: Text("no route found"),//todo move this strings to sm
  )
  )
  );
}
 }