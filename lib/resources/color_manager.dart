import 'package:flutter/material.dart';

class ColorManager {
 static Color lightprimary =Color.fromARGB(255, 83, 16, 16)   ;
 static Color primary  = Color.fromARGB(255, 165, 157, 144)  ;//التحويل الرقم للون منحط color(0xffورمز اللون)
 static Color darkgray = Color(0xff535c68) ;
  static Color black = Colors.black ;
 static Color darkred = Color(0xff93141d) ;
  static Color darkblue = Color(0xff512874) ;
  static Color white = Color(0xFFFFFFFF) ;
  static Color error = Color.fromARGB(255, 155, 33, 33) ;
  static Color  transparent = Colors.transparent;
  static Color gray=Color(0xFFF5F6F9);
  static Color amber = Colors.amberAccent;
  static Color lightBlack = Color(0xFF202020);
  static Color? green = Colors.green[700];
  static Color scaffoldBlack = Color(0xFF202020);

 static Color lightBlue = Color.fromARGB(255,37, 150, 190) ;
  static Color lightGrey = Color(0xFFe1e8f0);
  static Color lightGrey1 = Color(0xFFF3F5F8);
  static Color gold = Color(0xFFF3C54A);
}