const baseUrl = 'http://127.0.0.1:8000/api/';

const adminLoginPath = 'admin/login';
const adminLogoutPath = 'admin/logout';
const createLeaderPath = 'admin/leader/register';
const getAllLeadersPath = 'admin/leader/show';
const deleteLeaderPath = 'admin/leader/delete/?id=';
